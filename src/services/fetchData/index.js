import { api } from "../config";

export const getDataBranch = () => {
  return api("GET", `/BranchReps`);
};
