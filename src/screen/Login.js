import React, { useState } from "react";
import logo from "../images/logo192.png";
import Loading from "../component/PreLoader";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import { useHistory } from "react-router-dom";
import { useStateWithCallbackLazy } from "use-state-with-callback";
import { register } from "../services/authService";

const Login = (props) => {
  const [isName, setName] = useStateWithCallbackLazy("");
  const [nameError, setErrorName] = useStateWithCallbackLazy("");
  const [nameErrorBool, setErrorNameBool] = useStateWithCallbackLazy(true);
  const [isEmail, setEmail] = useStateWithCallbackLazy("");
  const [errorEmail, setErrorEmail] = useStateWithCallbackLazy("");
  const [errorEmailBool, setErrorEmailBool] = useStateWithCallbackLazy(true);
  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState("");
  const [open, setOpen] = React.useState(false);

  //Function handle perubahan input Username
  const handleChangeEmail = (event) => {
    setEmail(event.target.value.toString().trim(), (isEmail) => {
      if (isEmail.trim().length < 1) {
        setErrorEmail("Username is required.");
        setErrorEmailBool(true);
      } else {
        setErrorEmail("");
        setErrorEmailBool(false);
      }
    });
  };

  //Function handle perubahan input password
  const handleChangePassword = (event) => {
    setName(event.target.value.toString().trim(), (isName) => {
      if (isName.trim().length < 1) {
        setErrorName("Password is required.");
        setErrorNameBool(true);
      } else if (isName.trim().length < 8) {
        setErrorName("Password must be at least 8 characters.");
        setErrorNameBool(true);
      } else {
        setErrorName("");
        setErrorNameBool(false);
      }
    });
  };

  //Function handle Login
  const Create = (e) => {
    if (errorEmailBool !== true && isName !== "") {
      setLoading(true);
      register(isEmail, isName)
        .then((responseJson) => {
          const api = responseJson;
          if (api.success === true) {
            localStorage.setItem("token", api.token);
            setEmail("");
            setName("");
            Go_To();
          }
        })
        .catch((error) => {
          setEmail("");
          setName("");
          setError("Erro! Try again");
          setLoading(false);
          setOpen(true);
        });
    } else {
    }
  };

  const history = useHistory();
  const Go_To = () => {
    setTimeout(() => {
      let path = "/dashboard";
      history.push(path);
    }, 500);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div className="wrp-inner">
      <div className="wrp-login">
        <div className="header-logo-login">
          <div style={{ alignItems: "center", display: "flex" }}>
            <img src={logo} width="110" height="110" alt="icon" />
          </div>
        </div>
        <div noValidate autoComplete="off" className="row">
          <div className="label1">Username*</div>
          <TextField
            autoComplete="off"
            error={errorEmailBool}
            value={isEmail}
            onChange={handleChangeEmail}
            helperText={errorEmail}
            onBeforeInput={handleChangeEmail}
          />
          <div className="label2">Password*</div>
          <TextField
            autoComplete="off"
            error={nameErrorBool}
            defaultValue={isName}
            helperText={nameError}
            onChange={handleChangePassword}
          />
        </div>
      </div>

      <div className="button">
        <button onClick={Create} disabled={isLoading} className="row-button">
          Login
        </button>
        <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="error">
            {isError}
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
};
export default Login;
