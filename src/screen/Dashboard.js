import React, { useState, useEffect } from "react";
// Component
import logo from "../images/logo192.png";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Loading from "../component/PreLoader";
import { getDataBranch } from "../services/fetchData";

// Package
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import MUIDataTable from "mui-datatables";
import { ThemeProvider } from "@mui/styles";
import { createTheme, responsiveFontSizes } from "@mui/material/styles";

const Dashboard = (props) => {
  const [isLoading, setLoading] = useState(false);
  const [listData, setListData] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  //Function GetData
  const getData = () => {
    setLoading(true);

    getDataBranch()
      .then((response) => {
        if (response !== null) {
          setListData(response);
          setLoading(false);
        }
      })
      .catch((error) => {});
  };

  //Function Coloumn untuk table, package dari MUI
  const columns = [
    {
      name: "id",
      label: "No.",

      options: {
        customBodyRender: (value, tableMeta, update) => {
          let rowIndex = Number(tableMeta.rowIndex) + 1;
          return <>{rowIndex}.</>;
        },
      },
    },
    {
      name: "BranchName",
      label: "Branch Name",
    },
    {
      name: "BranchID",
      label: "Legal Name",
    },
    {
      name: "address",
      label: "Address",
    },
    {
      name: "email",
      label: "Email",
    },
  ];

  let theme = createTheme();
  theme = responsiveFontSizes(theme);

  if (isLoading) {
    return <Loading />;
  }

  //Function LogOut
  const handleLogOut = () => {
    localStorage.clear();
    let path = "/";
    window.location.replace(path);
  };

  return (
    <>
      <div className="wrp-outer">
        <div className="wrp-inner-result">
          <div className="header-result">
            <div className="header-logo-result">
              <img src={logo} width="110" height="110" alt="icon" />
            </div>
          </div>
          <div className="header-menu-result">
            <PopupState variant="popover" popupId="demo-popup-menu">
              {(popupState) => (
                <React.Fragment>
                  <Button variant="contained" {...bindTrigger(popupState)}>
                    Menu
                  </Button>
                  <Menu {...bindMenu(popupState)}>
                    <MenuItem onClick={handleLogOut}>Logout</MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
          </div>
          <div className="wrapper-result">
            <div className="header-title-result"></div>

            <ThemeProvider theme={theme}>
              <MUIDataTable
                title={"Branch"}
                columns={columns}
                data={listData?.map((item) => {
                  return [
                    "",
                    item.BranchName,
                    item.BranchDetails.LegalName,
                    item.BranchDetails.AddressLine1,
                    item.BranchDetails.Email,
                  ];
                })}
                options={{
                  selectableRows: false, // <===== will turn off checkboxes in rows
                }}
              />
            </ThemeProvider>

            <div className="button"></div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Dashboard;
