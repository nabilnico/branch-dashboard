import React from "react";
import { isLogin } from "../component/utils";
import { useHistory } from "react-router-dom";

const PrivateRoute = ({ component: Component, location, ...rest }) => {
  const history = useHistory();
  if (!isLogin() && location.pathname === `/dashboard`) {
    history.push("/");
    return null;
  }

  return <Component {...rest} />;
};

export default PrivateRoute;
