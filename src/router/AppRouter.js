import { Login, Dashboard } from "./Route";
import Wrapper from "../component/Wrapper";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from "./privateRoute";

function AppRouter() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Wrapper(Login)} />

        <PrivateRoute path="/dashboard" component={Dashboard} />
      </Switch>
    </Router>
  );
}

export default AppRouter;
