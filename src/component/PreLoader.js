import React from "react";
import "../App.css";

export default function PreLoader() {
  return (
    <div className="PreLoaderContainer">
      <img src="logo512.png" alt="icon" height="300" width="320" />
    </div>
  );
}
